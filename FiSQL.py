import mysql.connector as MySQL

class database(object):
    def __init__(self, host, user, password, db):
        self.dbConfig = {
            'user': user,
            'password': password,
            'host': host,
            'database': db
        }
        self.db = None
        self.dbCursor = None
        self.connected = False

    def connect(self):
        self.db = MySQL.connect(**self.dbConfig)
        self.dbCursor = self.db.cursor()
        self.connected = True

    def sql(self, command, variables):
        operationType = command.split(' ')[0]
        print(operationType)
        if self.connected is False:
            self.connect()
        if operationType == 'SELECT':
            self.dbCursor.execute(command, variables)
            query = self.dbCursor.fetchall()
            data = list()
            columns = self.dbCursor.column_names
            a = 0
            b = 0
            for row in query:
                data.append(dict())
                for piece in row:
                    data[a][columns[b]] = piece
                    b += 1
                a += 1
                b = 0
            return data
        elif operationType == 'INSERT':
            self.dbCursor.execute(command, variables)
            self.db.commit()
            return self.dbCursor.lastrowid
        elif operationType == 'UPDATE' or operationType == 'DELETE':
            self.dbCursor.execute(command, variables)
            self.db.commit()
            return self.dbCursor.rowcount
